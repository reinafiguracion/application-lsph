(function($) {
	$(document).ready(function() {
    $('.autonumeric').autoNumeric('init');
    $("#mmyy").inputmask("99/9999");
    $("#pin").inputmask("9999");

    $("#date").inputmask("99/99/9999");
    $("#phone").inputmask("(9999) 999-9999");
    $("#tin").inputmask("99-9999999");
    $("#ssn").inputmask("999-99-9999");
    $('#modal-phone').modal({
     	backdrop: 'static',
     	keyboard: true,
     	show: false
     })

     $('.md-loader').modal({
     	backdrop: 'static',
     	keyboard: true,
     	show: false
     })
  });
})(window.jQuery);